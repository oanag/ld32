﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {

	NavMeshAgent agent;

	void Start () {
		agent = GetComponent<NavMeshAgent> ();
	}

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition) - Camera.main.transform.position.y*Vector3.up;
			agent.SetDestination(target);
		}
	}
}
