﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CityGenerator : MonoBehaviour {

	const float BLOCK_WIDTH = 7;
	const float BLOCK_DIAG = 7*1.41f;
	const float ROAD_WIDTH = 2f;
	const float BUILDING_ROWS = 7;
	const float BUILDING_COLUMNS = 9;
	const int REMOVE_LINKS_MIN = 20;
	const int REMOVE_LINKS_MAX = 30;

	public GameObject cubePrefab;

	List<Node> nodes = new List<Node> ();
	List<Square> squares = new List<Square> ();

	void Start () {
		for (int i=0; i<BUILDING_COLUMNS; i++) {
			for (int j = 0; j<BUILDING_ROWS;j++){
				//Debug.DrawRay(new Vector3(i*5,0,j*5), Vector3.up,Color.red,1000);
				Square square = new Square();
				square.corners.Add (GetOrCreateNode(new Vector2(i*BLOCK_WIDTH,j*BLOCK_WIDTH)));
				square.corners.Add (GetOrCreateNode(new Vector2((i+1)*BLOCK_WIDTH,j*BLOCK_WIDTH)));
				square.corners.Add (GetOrCreateNode(new Vector2((i+1)*BLOCK_WIDTH,(j+1)*BLOCK_WIDTH)));
				square.corners.Add (GetOrCreateNode(new Vector2(i*BLOCK_WIDTH,(j+1)*BLOCK_WIDTH)));
				for (int ni = 0; ni<square.corners.Count; ni++){
					int ni2 = ni+1;
					if (ni2 == square.corners.Count) ni2 = 0;
					if (!square.corners[ni].neighbors.Contains(square.corners[ni2])){
						square.corners[ni].neighbors.Add (square.corners[ni2]);
					}
					if (!square.corners[ni2].neighbors.Contains(square.corners[ni])){
						square.corners[ni2].neighbors.Add (square.corners[ni]);
					}
				}
				squares.Add (square);
			}
		}
		RemoveExtremeEdges ();
		RandomRemoveLinks ();
		DrawBuildings ();
		//DebugDrawAllLinks ();
		Debug.Log ("DONE");
	}

	void RemoveExtremeEdges(){
		foreach (Node n in nodes) {
			if (Mathf.Approximately(n.pos.x, 0)){
				for (int i=0;i<n.neighbors.Count;i++){
					Node n2 =n.neighbors[i];
					if (Mathf.Approximately(n2.pos.x, 0)) {
						n.neighbors.Remove(n2);
						i--;
					}
				}
			}
			if (Mathf.Approximately(n.pos.x, BLOCK_WIDTH*BUILDING_COLUMNS)){
				for (int i=0;i<n.neighbors.Count;i++){
					Node n2 =n.neighbors[i];
					if (Mathf.Approximately(n2.pos.x, BLOCK_WIDTH*BUILDING_COLUMNS)) {
						n.neighbors.Remove(n2);
						i--;
					}
				}
			}
			if (Mathf.Approximately(n.pos.y, 0)){
				for (int i=0;i<n.neighbors.Count;i++){
					Node n2 =n.neighbors[i];
					if (Mathf.Approximately(n2.pos.y, 0)) {
						n.neighbors.Remove(n2);
						i--;
					}
				}
			}
			if (Mathf.Approximately(n.pos.y, BLOCK_WIDTH*BUILDING_ROWS)){
				for (int i=0;i<n.neighbors.Count;i++){
					Node n2 =n.neighbors[i];
					if (Mathf.Approximately(n2.pos.y, BLOCK_WIDTH*BUILDING_ROWS)) {
						n.neighbors.Remove(n2);
						i--;
					}
				}
			}
		}
	}

	void RandomRemoveLinks(){
		int linksToRemove = Random.Range (REMOVE_LINKS_MIN,REMOVE_LINKS_MAX);
		while (linksToRemove>0) {
			int index = Random.Range (0,nodes.Count);
			Node n1 = nodes[index];
			if (n1.neighbors.Count <= 2) continue;
			int nIndex = Random.Range (0,n1.neighbors.Count);

			Node n2 = n1.neighbors[nIndex];
			if (n2.neighbors.Count <= 2) continue;

			n1.neighbors.Remove(n2);

			n2.neighbors.Remove(n1);
			if (n1.neighbors.Count>0 && n2.neighbors.Count>0 && !PathExists(n1,n2)){				
				n1.neighbors.Add (n2);
				n2.neighbors.Add(n1);
			}else{
				Debug.DrawLine (new Vector3 (n1.pos.x, 1, n1.pos.y), new Vector3 (n2.pos.x, 1, n2.pos.y), Color.green, 1000);

				linksToRemove--;
			}
		}
	}

	bool PathExists(Node n1, Node n2){
		Queue<Node> queue = new Queue<Node> ();
		queue.Enqueue (n1);
		HashSet<Node> discovered = new HashSet<Node>();
		discovered.Add (n1);
		while (queue.Count>0) {
			Node node = queue.Dequeue();
			foreach (Node n in node.neighbors){
				if (n == n2) return true;
				if (!discovered.Contains(n)){
					queue.Enqueue(n);
					discovered.Add (n);
				}
			}
		}
		return false;
	}

	Node GetOrCreateNode(Vector2 pos){
		foreach (Node n in nodes) {
			if (n.pos == pos){
				return n;
			}
		}
		Node node = new Node ();
		node.pos = pos;
		nodes.Add (node);
		return node;
	}

	void DrawBuildings(){
		foreach (Square square in squares) {
			Rect r = new Rect();
			r.min = square.corners[0].pos;

			bool up=false,down=false,left=false,right=false;
			if (square.corners[0].neighbors.Contains(square.corners[1])){
				r.yMin += ROAD_WIDTH/2;
			}else{
				r.yMin -= ROAD_WIDTH/2;
				down = true;
			}
			if (square.corners[0].neighbors.Contains(square.corners[3])){
				r.xMin += ROAD_WIDTH/2;
			}else{
				r.xMin -= ROAD_WIDTH/2;
				left = true;
			}

			r.max = square.corners[2].pos;
			if (square.corners[2].neighbors.Contains(square.corners[1])){
				r.xMax -= ROAD_WIDTH/2;
			}else{
				r.xMax += ROAD_WIDTH/2;
				right = true;
			}

			if (square.corners[2].neighbors.Contains(square.corners[3])){
				r.yMax -= ROAD_WIDTH/2;
			}else{
				r.yMax += ROAD_WIDTH/2;
				up = true;
			}

			if (down && left){	
				r.xMin += ROAD_WIDTH;			
				r.yMin += ROAD_WIDTH;
				down = false;
				left = false;
			}
			if (up&&right){	
				r.xMax -= ROAD_WIDTH;			
				r.yMax -= ROAD_WIDTH;
				up = false;
				right = false;
			}
			
			if (up && left){	
				r.xMin += ROAD_WIDTH;			
				r.yMax -= ROAD_WIDTH;
			}

			if (down&&right){	
				r.xMax -= ROAD_WIDTH;			
				r.yMin -= ROAD_WIDTH;
			}

			GameObject building = (GameObject) Instantiate (cubePrefab);
			building.transform.SetParent(this.transform);
			building.transform.position = new Vector3(r.center.x,0.5f,r.center.y);
			building.transform.localScale = new Vector3(r.width,1,r.height);
		}
	}
	/*
	int index =0;
	void OnGUI(){
		foreach (Node n2 in nodes[index].neighbors){
			Debug.DrawLine (new Vector3 (nodes[index].pos.x, 1, nodes[index].pos.y), new Vector3 (n2.pos.x, 1, n2.pos.y), Color.green);
		}
		if (GUI.Button (new Rect (0, 0, 100, 100), "Next")) {
			index++;
		}
	}*/

	void DebugDrawAllLinks(){
		foreach (Node n in nodes) {
			foreach (Node n2 in n.neighbors){
			Debug.DrawLine (new Vector3 (n.pos.x, 1, n.pos.y), new Vector3 (n2.pos.x, 1, n2.pos.y), Color.green, 1000);
			}
		}
	}

	class Node{
		public Vector2 pos;
		public List<Node> neighbors = new List<Node>();
	}

	class Square{
		public List<Node> corners = new List<Node>();
	}
}
