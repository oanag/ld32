﻿using UnityEngine;
using System.Collections;

public class ForceAspectRatio : MonoBehaviour {
	const float TARGET_RATIO = 1024/768f;

	void Start () {
		Camera camera = GetComponent<Camera> ();
		Rect rect = camera.pixelRect;
		Vector2 oldCenter = rect.center;
		rect.width = TARGET_RATIO * rect.height;
		rect.center = oldCenter;
		camera.pixelRect = rect;
	}
}
